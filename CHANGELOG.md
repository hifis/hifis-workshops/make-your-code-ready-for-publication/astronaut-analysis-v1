<!--
SPDX-FileCopyrightText: 2018 German Aerospace Center (DLR)
SPDX-License-Identifier: MIT
-->

# Changelog

All notable changes will be documented in this file.

## [2021-03-17]

This release marks the latest version of the initially used code example.
The code example focuses on a re-usable data analytics script rather than a specific data publication.
It has been used since 2018 as a step-by-step example to show researchers how to make their research code ready for citation in a scientific publication.

[2021-03-17]: https://gitlab.com/hifis/hifis-workshops/make-your-code-ready-for-publication/astronaut-analysis-v1/-/tags/2021-03-17
